# Introduction:

Welcome to **Autoremover** project and book.
Here you will find documentation, examples and the vision of this project.

> Feel free to suggest any change or improvement through the Issue system.

## Requirements

Just **python3**, at least v3.6 or greater.

## Installation

If you want to make some changes to the code, follow this steps:

**ssh**:
```sh
git clone git@gitlab.com:kfelsner/autoremover.git
```

**https**:
```sh
git clone https://gitlab.com/kfelsner/autoremover.git
```

If only want to download the main file and use it, then follow simple wget command:

**wget**
```sh
wget https://gitlab.com/kfelsner/autoremover/-/raw/master/autoremover.py
```

And then execute the file with a path.

## Execution
Simply run the file with proper user permissions as follows:
> In my case I have the file inside /opt folder.
```sh
/opt/./autoremover.py -p <path to folder>
```

You can add as many folders as wanted, like this:

```sh
/opt/./autoremover.py -p <path> -p <another_path>
```

**It's mandatory** that the path you specify is a dir path, not a file.
Whatever, the program itself will check the path just in case.

## Configuration
The only configuration you need it's to add one of the following tag at the end of a file:

* _nm => for n **m**inutes before deleting the file.
* _nh => for n **h**ours before deleting the file.
* _nd => for n **d**ays before deleting the file.
* _nw => for n **w**eeks before deleting the file.
* _nM => for n **M**onths before deleting the file.

> After that tag the extension comes in leaving a file like this: my_file_1d.txt

Once you some file configured like that run the program and it will evaluate wether or not remove the file.

**Disclaimer**: hidden files like `.something` won't be evaluated.

## Automation
In order to automate the file to run it in certain folder every X times or a certain time of day you could do the follow:

First put the file at you scripts or program folder. In my case I will put the file `autoremover.py` at /opt/.

Then do `crontab -e` to start editing the crons. This will show an editor to edit the active user cron jobs.
> More information about crons at [Wikipedia](https://en.wikipedia.org/wiki/Cron).

If you want to run the program every 5 minutes put this line inside the editor:

`*/5 * * * * /path/to/script-or-program <parameters>`

In my case:

`*/5 * * * * /opt/./autoremover.py -p <path to folder>`

Save and that's all you need to automate the program.

**Disclaimer**: crons are only available at Unix-like systems. If you use a Windows or another non Unix OS you should look for "how to automate X program" at the Internet.

> [Here][1] is a page where you can take cron configuration easy.

[1]: https://crontab.guru/
