#!/usr/bin/python3
from datetime import datetime
import os
import click

# Contants ---
CURRENT_TIME = datetime.now()

# Validators ---
def same_date_validator(created: datetime) -> bool:
    return CURRENT_TIME.date() == created.date()

def same_month_validator(created: datetime) -> bool:
    return CURRENT_TIME.month == created.month

def same_year_validator(created: datetime) -> bool:
    return CURRENT_TIME.year == created.year

def min_validator(n: int, created: datetime) -> bool:
    if not same_date_validator(created):
        return True
    return (CURRENT_TIME.minute - created.minute) >= n

def hour_validator(n: int, created: datetime) -> bool:
    if not same_date_validator(created):
        return True
    return (CURRENT_TIME.hour - created.hour) >= n

def days_validator(n: int, created: datetime) -> bool:
    if not same_month_validator(created) or not same_year_validator(created):
        return True
    return (CURRENT_TIME.day - created.day) >= n

def weeks_validator(n: int, created: datetime) -> bool:
    diff = (CURRENT_TIME - created).days / 7
    return diff >= n

def months_validator(n: int, created: datetime) -> bool:
    if not same_year_validator(created):
        return True
    return (CURRENT_TIME.month - created.month) >= n

VALIDATORS = {
    'm': min_validator,
    'h': hour_validator,
    'd': days_validator,
    'w': weeks_validator,
    'M': months_validator
}

# Getters ---
def get_creation_time(path_to_file: str) -> datetime:
    created = datetime.fromtimestamp(os.path.getctime(path_to_file))
    return created

def get_files_from(path):
    """
    Takes a directory and retrieve all files.
    :params path: string pointing to directory.
    """
    # Code.

def get_tag_from(f) -> str:
    """
    Recieve a file and return the tag.
    :params f: file name.
    """
    base = f.split('_')[-1].split('.')[0]
    tag = None
    num = None
    if base and len(base[-1]) == 1 and base[-1] in list(VALIDATORS.keys()):
        tag = base[-1]
    if base[:-1].isdigit():
        num = base[:-1]
    return num, tag

# Deleters ---
def delete_file(path, f) -> bool:
    """
    Deletes the file.
    :params path: string pointing to directory.
    :params f: the file name.
    """
    # Code.

# Main ---
@click.command()
@click.option('-p', '--paths', multiple=True)
def remove(paths = None):
    """
    Main function that handles the discover and deletion of a files.
    :params path: str of the directory to look at.
    """
    for path in paths:
        # Step 1: Get a valid path.
        if not path or not os.path.isdir(path):
            click.echo('Please enter a path to start')
            return
        # Step 2: Check all the files in that folder. (Not recursive).
        file_list = os.listdir(path)
        # Step 3: Check the tag and creation date of each file.
        for f in file_list:
            full_path = path + '/' + f
            num, tag = get_tag_from(f)
            if not num or not tag:
                continue
            # Step 4: Depending on the extension call the validator function.
            created = get_creation_time(full_path)
            # Step 5: If validator function returns False, then delete.
            queue_to_remove = VALIDATORS[tag](int(num), created)
            if queue_to_remove:
                os.remove(full_path)

if __name__ == '__main__':
    remove()
